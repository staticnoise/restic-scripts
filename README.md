# Backblaze B2 backup with restic

## Initialize new repository

Great articles: [1][1], [2][2], [3][3]

```bash
sudo dnf install s3fs-fuse
```

## Setup

```bash
cp example-env.sh env.sh
source env.sh
restic snapshots
```

<!-- links -->

[1]: https://www.seanh.cc/2022/04/03/restic/#7-source-the-env-script
[2]: https://help.backblaze.com/hc/en-us/articles/4403944998811-Quickstart-Guide-for-Restic-and-Backblaze-B2-Cloud-Storage
[3]: https://blog.dalydays.com/post/2021-10-20-using-restic-with-backblaze-b2/
