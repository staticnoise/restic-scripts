#!/usr/bin/env bash

set -euo pipefail

source ./env.sh

restic mount /mnt/backblaze
