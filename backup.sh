#!/usr/bin/env bash

set -euo pipefail

source ./env.sh

restic backup \
    --exclude-caches \
    --one-file-system \
    --cleanup-cache \
    --exclude node_modules \
    --exclude venv \
    --exclude .next/cache \
    $RESTIC_INCLUDE
